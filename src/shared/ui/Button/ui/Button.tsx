import { type ButtonHTMLAttributes, type FC } from 'react'

import cls from './Button.module.scss'
import { classNames } from '@/shared/lib/classNames/classNames'

type Variant = 'primary' | 'icon'

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    variant?: Variant
    disabled?: boolean
}

export const Button: FC<ButtonProps> = (props) => {
    const {
        variant,
        children,
        disabled = false,
        className,
        ...otherProps
    } = props
    return (
        <button
            type="button"
            className={classNames(cls.btn, {}, [cls[variant], className])}
            disabled={disabled}
            {...otherProps}
        >
            {children}
        </button>
    )
}
