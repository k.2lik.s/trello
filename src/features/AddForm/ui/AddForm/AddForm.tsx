import { Button } from '@/shared/ui/Button'
import { Input } from '@/shared/ui/Input'
import { useDispatch, useSelector } from 'react-redux'
import { memo, useCallback } from 'react'
import { boardActions, getTitle } from '@/entities/Board'
import cls from './AddForm.module.scss'

interface AddFormProps {
    onClose: () => void
}

const AddForm = memo(({ onClose }: AddFormProps) => {
    const dispatch = useDispatch()
    const title = useSelector(getTitle)

    const onChangeTitle = useCallback(
        (value: string) => {
            dispatch(boardActions.setFormTitle(value))
        },
        [dispatch]
    )

    const onLoginClick = useCallback(() => {
        dispatch(boardActions.setForm())
        onClose()
    }, [dispatch, title])

    return (
        <div className={cls.AddForm}>
            <Input placeholder="Title" onChange={onChangeTitle} value={title} />
            <Button onClick={onLoginClick} variant="primary" disabled={!title}>
                Add
            </Button>
        </div>
    )
})

export default AddForm
