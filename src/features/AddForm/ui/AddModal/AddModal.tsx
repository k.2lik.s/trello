import { Modal } from '@/shared/ui/Modal/ui/Modal'
import AddForm from '../AddForm/AddForm'

interface AddModalProps {
    className?: string
    isOpen: boolean
    onClose: () => void
}

export const AddModal = ({ className, isOpen, onClose }: AddModalProps) => (
    <Modal className={className} isOpen={isOpen} onClose={onClose}>
        <AddForm onClose={onClose} />
    </Modal>
)
