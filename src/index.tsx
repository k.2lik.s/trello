import { createRoot } from 'react-dom/client'
import { StoreProvider } from './app/providers/StoreProvider'
import App from './app/App'

const rootElement = document.getElementById('root') as HTMLElement
const root = createRoot(rootElement)

root.render(
    <StoreProvider>
        <App />
    </StoreProvider>
)
