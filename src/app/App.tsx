import { Board } from '@/widgets/Board'
import './styles/index.scss'

const App = () => {
    return <Board />
}

export default App
