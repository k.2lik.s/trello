import { PayloadAction, createSlice, nanoid } from '@reduxjs/toolkit'
import { BoardSchema, ICard, IColumn } from '../types/boardSchema.type'

const initialState: BoardSchema = {
    list: [
        {
            id: nanoid(),
            title: 'Mock Column',
            items: [{ id: nanoid(), title: 'Mock Card' }],
        },
    ],
    currentColumn: null,
    currentCard: null,
    form: {
        id: null,
        title: '',
    },
}

export const boardSlice = createSlice({
    name: 'board',
    initialState,
    reducers: {
        setCurrentColumn: (state, action: PayloadAction<IColumn>) => {
            state.currentColumn = action.payload
        },
        setCurrentCard: (state, action: PayloadAction<ICard>) => {
            state.currentCard = action.payload
        },
        setMove: (
            state,
            action: PayloadAction<{ column: IColumn; card: ICard }>
        ) => {
            const { card, column } = action.payload
            const { list, currentCard, currentColumn } = state
            const stateCol = JSON.parse(JSON.stringify(column))
            const newCurrCol = JSON.parse(JSON.stringify(currentColumn))

            const holdCardIdx = newCurrCol.items.findIndex(
                (c: ICard) => c.id === currentCard.id
            )
            newCurrCol.items.splice(holdCardIdx, 1)

            if (card) {
                if (holdCardIdx >= 0 && column.id === currentColumn.id) {
                    stateCol.items.splice(holdCardIdx, 1)
                }
                const dropCardIdx = stateCol.items.findIndex(
                    (c: ICard) => c.id === card.id
                )
                stateCol.items.splice(dropCardIdx, 0, currentCard)
            } else {
                const dropCardIdx = stateCol.items.findIndex(
                    (c: ICard) => c.id === currentCard.id
                )
                if (dropCardIdx >= 0) {
                    stateCol.items.splice(dropCardIdx, 1)
                }
                stateCol.items.push(currentCard)
            }

            const newList = list.map((item) => {
                if (item.id === stateCol.id) {
                    return stateCol
                }
                if (item.id === newCurrCol.id) {
                    return newCurrCol
                }
                return item
            })

            state.list = newList
        },
        setFormId: (state, action: PayloadAction<string>) => {
            state.form.id = action.payload
        },
        setFormTitle: (state, action: PayloadAction<string>) => {
            state.form.title = action.payload
        },
        setForm: (state) => {
            const form = state.form
            const list: IColumn[] = Object.assign(state.list)

            if (form?.id) {
                list.find((c) => c.id === state.form.id).items.push({
                    id: nanoid(),
                    title: form?.title,
                })
            } else {
                list.push({
                    id: nanoid(),
                    title: form?.title,
                    items: [],
                })
            }

            state.list = list
        },
        setFormClear: (state) => {
            state.form = {
                id: null,
                title: '',
            }
        },
        setUpdateColumn: (state, action: PayloadAction<IColumn>) => {
            const newColumn = action.payload
            const newState: BoardSchema = Object.assign(state)
            newState.list.find((c) => c.id === newColumn.id).title =
                newColumn.title

            state = newState
        },
        setUpdateCard: (
            state,
            action: PayloadAction<{ columnId: string; card: ICard }>
        ) => {
            const newCard = action.payload.card
            const newState: BoardSchema = Object.assign(state)
            newState.list
                .find((c) => c.id === action.payload.columnId)
                .items.find((card) => card.id === newCard.id).title =
                newCard.title

            state = newState
        },
    },
})

export const { actions: boardActions, reducer: boardReducer } = boardSlice
