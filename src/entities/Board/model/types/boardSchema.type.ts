export interface ICard {
    id?: string
    title: string
}

export interface IColumn {
    id: string
    title: string
    items: ICard[]
}

export interface BoardSchema {
    list: IColumn[]
    currentColumn: IColumn | null
    currentCard: ICard | null
    form: ICard
}
