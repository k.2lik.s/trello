import { BoardSchema } from '../../types/boardSchema.type'

export const getCard = (state: { board: BoardSchema }) =>
    state.board.currentCard
