import { BoardSchema } from '../../types/boardSchema.type'

export const getColumn = (state: { board: BoardSchema }) =>
    state.board.currentColumn
