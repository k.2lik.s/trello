import { BoardSchema } from '../../types/boardSchema.type'

export const getBoard = (state: { board: BoardSchema }) => state.board
