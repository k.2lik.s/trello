import { BoardSchema } from '../../types/boardSchema.type'

export const getTitle = (state: { board: BoardSchema }) =>
    state?.board?.form?.title || ''
