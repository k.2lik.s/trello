import { DragEvent, useCallback, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { IColumn, ICard, getBoard, boardActions } from '@/entities/Board'
import { Column } from '../Column/Column'
import cls from './Board.module.scss'
import { AddModal } from '@/features/AddForm/ui/AddModal/AddModal'
import { Button } from '@/shared/ui/Button'

export const Board = () => {
    const dispatch = useDispatch()
    const { list } = useSelector(getBoard)
    const [isShowModal, setIsShowModal] = useState(false)

    const onToggleModal = useCallback((column?: IColumn) => {
        if (column?.id) {
            dispatch(boardActions.setFormId(column.id))
        } else {
            dispatch(boardActions.setFormClear())
        }
        setIsShowModal((prev) => !prev)
    }, [])

    const setCurrentColumn = (column: IColumn) =>
        dispatch(boardActions.setCurrentColumn(column))
    const setCurrentCard = (card: ICard) =>
        dispatch(boardActions.setCurrentCard(card))
    const setMove = (column: IColumn, card: ICard) =>
        dispatch(boardActions.setMove({ column, card }))

    const hndlDragStart = (board: IColumn, card: ICard) => {
        setCurrentCard(card)
        setCurrentColumn(board)
    }

    const hndlDrop = (
        e: DragEvent<HTMLDivElement>,
        column: IColumn,
        card?: ICard
    ) => {
        e.stopPropagation()
        setMove(column, card)
        e.currentTarget.classList.remove('shadow')
    }

    return (
        <div className={cls.list}>
            {list.map((column) => (
                <div key={column.id} className={cls.list__item}>
                    <Column
                        column={column}
                        onDragStart={hndlDragStart}
                        onDrop={hndlDrop}
                        onShowMoadal={() => onToggleModal(column)}
                    />
                </div>
            ))}
            <div className={cls.list__item}>
                <Button onClick={() => onToggleModal()} variant="primary">
                    Add column
                </Button>
            </div>
            {isShowModal && (
                <AddModal isOpen={isShowModal} onClose={onToggleModal} />
            )}
        </div>
    )
}
