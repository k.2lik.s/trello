// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'card': string;
  'focus': string;
  'hide': string;
  'shadow': string;
}
export const cssExports: CssExports;
export default cssExports;
