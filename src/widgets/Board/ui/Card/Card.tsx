import { DragEvent, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { ICard, boardActions, getCard } from '@/entities/Board'
import cls from './Card.module.scss'
import { Input } from '@/shared/ui/Input'
import { Button } from '@/shared/ui/Button'

interface CardProps {
    card?: ICard
    columnId?: string
    onDragStart: (e: DragEvent<HTMLDivElement>) => void
    onDrop: (e: DragEvent<HTMLDivElement>) => void
}

export const Card = (props: CardProps) => {
    const { card, onDragStart, onDrop, columnId } = props
    const currentCard = useSelector(getCard)
    const dispatch = useDispatch()
    const [edit, setEdit] = useState(false)
    const [title, setTitle] = useState(card.title)

    const hndlDragOver = (e: DragEvent<HTMLDivElement>, card: ICard) => {
        e.preventDefault()
        e.stopPropagation()
        if (e.currentTarget.className === cls.card) {
            if (card.id !== currentCard.id) {
                e.currentTarget.classList.add(cls.shadow)
            } else {
                e.currentTarget.classList.add(cls.focus)
            }
        }
    }

    const hndlRemoveCls = (e: DragEvent<HTMLDivElement>) => {
        e.currentTarget.classList.remove(cls.shadow)
        if (card.id === currentCard.id) {
            e.currentTarget.classList.add(cls.hide)
        }
    }
    const hndlRemoveAllCls = (e: DragEvent<HTMLDivElement>) => {
        e.currentTarget.classList.remove(cls.shadow)
        e.currentTarget.classList.remove(cls.focus)
        e.currentTarget.classList.remove(cls.hide)
    }

    const hndlEdit = () => {
        setEdit(true)
    }
    const hndlSave = () => {
        dispatch(
            boardActions.setUpdateCard({
                columnId,
                card: { ...card, title: title },
            })
        )
        setEdit(false)
    }

    return (
        <div
            onDragStart={onDragStart}
            onDragOver={(e: DragEvent<HTMLDivElement>) => hndlDragOver(e, card)}
            onDragLeave={hndlRemoveCls}
            onDrop={(e: DragEvent<HTMLDivElement>) => {
                hndlRemoveAllCls(e)
                onDrop(e)
            }}
            onDragEnd={hndlRemoveAllCls}
            draggable
            key={card.id}
            className={cls.card}
        >
            {edit ? (
                <>
                    <Input
                        value={title}
                        className="mr-2 p-2"
                        onChange={setTitle}
                    />
                    <Button variant="icon" onClick={hndlSave} className="py-2">
                        Save
                    </Button>
                </>
            ) : (
                <p onClick={hndlEdit}>{card.title}</p>
            )}
        </div>
    )
}
