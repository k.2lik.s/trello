import { DragEvent, useState } from 'react'
import { ICard, IColumn, boardActions } from '@/entities/Board'
import { Card } from '../Card/Card'
import cls from './Column.module.scss'
import { Button } from '@/shared/ui/Button'
import { useDispatch } from 'react-redux'
import { Input } from '@/shared/ui/Input'

interface ColumnProps {
    column?: IColumn
    onDragStart: (board: IColumn, card: ICard) => void
    onDrop: (
        e: DragEvent<HTMLDivElement>,
        column: IColumn,
        card?: ICard
    ) => void
    onShowMoadal: () => void
}

export const Column = (props: ColumnProps) => {
    const { column, onDragStart, onDrop, onShowMoadal } = props
    const dispatch = useDispatch()
    const [edit, setEdit] = useState(false)
    const [title, setTitle] = useState(column.title)

    const hndlDragOver = (e: DragEvent<HTMLDivElement>) => {
        e.preventDefault()
        if (e.currentTarget.className === cls.column) {
            e.currentTarget.classList.add(cls.shadow)
        }
    }

    const hndlRemoveCls = (e: DragEvent<HTMLDivElement>) => {
        e.preventDefault()
        e.currentTarget.classList.remove(cls.shadow)
    }

    const hndlEdit = () => {
        setEdit(true)
    }
    const hndlSave = () => {
        dispatch(boardActions.setUpdateColumn({ ...column, title: title }))
        setEdit(false)
    }

    return (
        <div
            className={cls.column}
            onDragOver={hndlDragOver}
            onDrop={(e: DragEvent<HTMLDivElement>) => {
                hndlRemoveCls(e)
                onDrop(e, column)
            }}
            onDragLeave={hndlRemoveCls}
        >
            <div className={cls.head}>
                {edit ? (
                    <>
                        <Input
                            value={title}
                            className="mr-2 p-2"
                            onChange={setTitle}
                        />
                        <Button
                            variant="icon"
                            onClick={hndlSave}
                            className="py-2"
                        >
                            Save
                        </Button>
                    </>
                ) : (
                    <>
                        <h4 className={cls.title} onClick={hndlEdit}>
                            {column.title}
                        </h4>
                        <Button variant="icon" onClick={onShowMoadal}>
                            +
                        </Button>
                    </>
                )}
            </div>
            {column?.items?.map((card) => (
                <Card
                    key={card.id}
                    card={card}
                    onDragStart={() => onDragStart(column, card)}
                    onDrop={(e: DragEvent<HTMLDivElement>) =>
                        onDrop(e, column, card)
                    }
                    columnId={column.id}
                />
            ))}
        </div>
    )
}
