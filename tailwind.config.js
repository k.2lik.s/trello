/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.{ts,tsx}',
  ],
  corePlugins: {
    preflight: false,
  },
  theme: {
    fontFamily: {
      sans: ['Courier New', 'Arial', 'sans-serif']
    },
    extend: {
    },
    container: {
    },
    variants: {},
    future: {
      removeDeprecatedGapUtilities: true,
      purgeLayersByDefault: true,
    },
  },
};
